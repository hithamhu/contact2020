package com.hithamsoft.learnapp.contacts2020.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.databinding.ContactDataBinding;
import com.hithamsoft.learnapp.contacts2020.fragment.ContactDetails;
import com.hithamsoft.learnapp.contacts2020.model.Contact;

import java.util.List;

public class ContactAdapter extends

        RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private static final String TAG = ContactAdapter.class.getSimpleName();



    private Context context;

    private List<Contact> list;

    private OnItemClickListener onItemClickListener;

    public ContactAdapter(Context context, List<Contact> list) {

        this.context = context;

        this.list = list;

        this.onItemClickListener = onItemClickListener;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ContactDataBinding contactDataBinding;

        public ViewHolder(ContactDataBinding contactDataBinding) {

            super(contactDataBinding.getRoot());
            this.contactDataBinding=contactDataBinding;

        }

        public void bind(final Contact model,

                         final OnItemClickListener listener) {
            this.contactDataBinding.setContactModel(model);


        }
        public ContactDataBinding getContactDataBinding(){
            return contactDataBinding;
        }

    }

    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);
        ContactDataBinding contactDataBinding=ContactDataBinding.inflate(inflater,parent,false);
        return new ViewHolder(contactDataBinding);

    }

    @Override

    public void onBindViewHolder(ViewHolder holder, final int position) {

        Contact item = list.get(position);

        holder.bind(item, onItemClickListener);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactDetails bottomSheetFragment = new ContactDetails();
                Bundle bundle=new Bundle();
                bundle.putParcelable("ContactItem",list.get(position));
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(((FragmentActivity)context).getSupportFragmentManager(), bottomSheetFragment.getTag());
                //Toast.makeText(context,"cliced",Toast.LENGTH_SHORT).show();
            }
        });
//

    }

    @Override

    public int getItemCount() {

        return list.size();

    }

    public interface OnItemClickListener {

        void onItemClick();

    }

}