package com.hithamsoft.learnapp.contacts2020.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.activites.MainActivity;
import com.hithamsoft.learnapp.contacts2020.databinding.FragmentContactDetailsBinding;
import com.hithamsoft.learnapp.contacts2020.model.Contact;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactDetails extends BottomSheetDialogFragment implements View.OnClickListener {
    private static final String TAG = "ContactDetails";
    FragmentContactDetailsBinding detailsBinding;
    Contact contact;
    boolean isEdit = false;
    View sanckView;
    ProgressDialog progressDialog;
    public ContactDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        detailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_details, container, false);
       sanckView=detailsBinding.frameContact;
        Bundle bundle = new Bundle();
        if (getArguments() != null) {
            contact = getArguments().getParcelable("ContactItem");
            getDetails();
        }
        detailsBinding.contactCallBtn.setOnClickListener(this);
        detailsBinding.contactDeleteBtn.setOnClickListener(this);
        detailsBinding.contactEditBtn.setOnClickListener(this);
        detailsBinding.contactEmailBtn.setOnClickListener(this);
        detailsBinding.contactSubmitBtn.setOnClickListener(this);


        return detailsBinding.getRoot();
    }

    private void getDetails() {
        detailsBinding.contactChar.setText(contact.getNameChar());
        detailsBinding.contactNamedetailsTxt.setText(contact.getName());
        detailsBinding.contactName.setText(contact.getName());
        detailsBinding.contactEmail.setText(contact.getEmail());
        detailsBinding.contactNumber.setText(contact.getNumber());
        Log.d(TAG, "getDetails: contact: " + contact.getName() + "\n" +
                contact.getEmail() + "\n" +
                contact.getNumber() + "\n");
    }


    @Override
    public void onClick(View v) {
        int buttonId = v.getId();
        if (buttonId == detailsBinding.contactCallBtn.getId()) {
            callNumber();

        } else if (buttonId == detailsBinding.contactEditBtn.getId()) {

            editContact();
        } else if (buttonId == detailsBinding.contactEmailBtn.getId()) {
            sendEmail();
        } else if (buttonId == detailsBinding.contactDeleteBtn.getId()) {
            deleteContact();
        } else if (buttonId == detailsBinding.contactSubmitBtn.getId()) {
            submitContactEdit();
        }

    }

    private void submitContactEdit() {
      progressDialog  =new ProgressDialog(getContext());
        progressDialog.show();
        String name=detailsBinding.contactName.getText().toString();
        String email= detailsBinding.contactEmail.getText().toString();
        String number= detailsBinding.contactNumber.getText().toString();
      if (name.isEmpty()||
             number.isEmpty()||
             email.isEmpty()){
          Snackbar.make(sanckView,"Please Enter All deails",Snackbar.LENGTH_SHORT).show();
          progressDialog.dismiss();
      }else {
         contact.setName(name);
         contact.setNumber(number);
         contact.setEmail(email);
          Backendless.Persistence.save(contact, new AsyncCallback<Contact>() {
              @Override
              public void handleResponse(Contact response) {
                  Snackbar.make(sanckView,"Update submit",Snackbar.LENGTH_SHORT).show();
                  detailsBinding.contactChar.setText(response.getNameChar());
                  detailsBinding.contactNamedetailsTxt.setText(response.getName());
                  progressDialog.dismiss();

                  MainActivity.contactAdapter.notifyDataSetChanged();
              }

              @Override
              public void handleFault(BackendlessFault fault) {
                  Snackbar.make(sanckView,"Errot: "+fault.getMessage(),Snackbar.LENGTH_SHORT).show();
                  progressDialog.dismiss();

              }
          });
      }
    }

    private void deleteContact() {
        final AlertDialog.Builder alerteDialog=new AlertDialog.Builder(getContext());
        alerteDialog.setTitle("Delete contact");
        alerteDialog.setMessage("Are You sure you want to delete contact?");
        alerteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Backendless.Persistence.of(Contact.class).remove(contact, new AsyncCallback<Long>() {
                    @Override
                    public void handleResponse(Long response) {
                        Snackbar.make(sanckView,"Contact Removed" ,Snackbar.LENGTH_SHORT).show();
                        MainActivity.contactAdapter.notifyDataSetChanged();
                        MainActivity.contactList.remove(contact);
                        dismiss();

                        ContactDetails.this.dismiss();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Snackbar.make(sanckView,"Errot: "+fault.getMessage(),Snackbar.LENGTH_SHORT).show();
                    }
                });

            }
        });
        alerteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        alerteDialog.show();
    }

    private void sendEmail() {
        Toast.makeText(getContext(), "Email Send", Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL,contact.getEmail());
        startActivity(Intent.createChooser(intent,"Send Email to: "+contact.getEmail()));

    }

    private void editContact() {
        isEdit = !isEdit;
        if (isEdit) {
            detailsBinding.editFormContainer.setVisibility(View.VISIBLE);
        }else {
            detailsBinding.editFormContainer.setVisibility(View.GONE);

        }
    }

    private void callNumber() {
        Toast.makeText(getContext(), "Calling", Toast.LENGTH_SHORT).show();
        String url="tel:"+contact.getNumber();
        Intent intent=new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(url));
        startActivity(intent);

    }
}
