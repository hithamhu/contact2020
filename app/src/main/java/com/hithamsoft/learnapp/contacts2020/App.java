package com.hithamsoft.learnapp.contacts2020;

import android.app.Application;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

public class App extends Application {
    public static final String APPLICATION_ID = "69D1F500-2781-E007-FF2F-7D2B8D8A9800";
    public static final String API_KEY = "57909943-9BFE-489E-AC49-CB480E1E8ABB";
    public static final String SERVER_URL = "https://api.backendless.com";

    public static BackendlessUser user;


    @Override
    public void onCreate() {
        super.onCreate();
        Backendless.setUrl(SERVER_URL);
        Backendless.initApp(getApplicationContext(),APPLICATION_ID,API_KEY);
    }
}
