package com.hithamsoft.learnapp.contacts2020.activites;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.hithamsoft.learnapp.contacts2020.App;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.databinding.ActivityRegisterBinding;
import com.hithamsoft.learnapp.contacts2020.severside.UserInfoService;

public class Register extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Register";
    Intent startSendService;

    ActivityRegisterBinding registerBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        registerBinding.registerSignUpBtn.setOnClickListener(this);
        registerBinding.signInBtn.setOnClickListener(this);
        startSendService = new Intent(this, UserInfoService.class);


//to validate is user login or not

        Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {
            @Override
            public void handleResponse(Boolean response) {
                if (response) {
                    showProgress(true);
                    String userObject = UserIdStorageFactory.instance().getStorage().get();
                    Backendless.Data.of(BackendlessUser.class).findById(userObject, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            App.user = response;
                            showProgress(false);
                            if (ActivityCompat.checkSelfPermission(Register.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                startForegroundService(new Intent(Register.this, UserInfoService.class));
                            } else {
                                startService(new Intent(Register.this, UserInfoService.class));
                            }
                            Log.d(TAG, "handleResponse: service start");
                            startActivity(new Intent(Register.this, MainActivity.class));
                            finish();

                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            showProgress(false);
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });

    }

    private boolean validateForm() {
        boolean valid = true;

        String userName = registerBinding.registerUserNameTxt.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            registerBinding.registerUserNameTxt.setError("Required.");
            valid = false;
        } else {
            registerBinding.registerUserNameTxt.setError(null);
        }


        String email = registerBinding.registerEmailTxt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            registerBinding.registerEmailTxt.setError("Required.");
            valid = false;
        } else {
            registerBinding.registerEmailTxt.setError(null);
        }

        String password = registerBinding.registerPasswordTxt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            registerBinding.registerPasswordTxt.setError("Required.");
            valid = false;
        } else {
            registerBinding.registerPasswordTxt.setError(null);
        }

        return valid;
    }


    private void registerProcess() {
        if (!validateForm()) {
            return;
        }


        String userName = registerBinding.registerUserNameTxt.getText().toString();
        String userEmail = registerBinding.registerEmailTxt.getText().toString();
        String userPassword = registerBinding.registerPasswordTxt.getText().toString();

        BackendlessUser user = new BackendlessUser();
        user.setEmail(userEmail);
        user.setPassword(userPassword);
        user.setProperty("name", userName);
        //show progress
        showProgress(true);
        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                showProgress(false);
                startActivity(new Intent(Register.this, Login.class));
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(Register.this, "Something Wrong :( \n Error is: " + fault.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "handleFault: " + fault.getMessage());
                showProgress(false);

            }
        });

    }

    private void sendData() {

        startService(startSendService);

    }


    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        registerBinding.registerForm.setVisibility(show ? View.GONE : View.VISIBLE);
        registerBinding.registerForm.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                registerBinding.registerForm.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        registerBinding.registerProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        registerBinding.registerProgressBar.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                registerBinding.registerProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == registerBinding.signInBtn.getId()) {
            startActivity(new Intent(Register.this, Login.class));

        } else if (v.getId() == registerBinding.registerSignUpBtn.getId()) {
            registerProcess();
        }

    }


}
