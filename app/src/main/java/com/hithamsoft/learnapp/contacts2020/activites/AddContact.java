package com.hithamsoft.learnapp.contacts2020.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.hithamsoft.learnapp.contacts2020.App;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.databinding.ActivityAddContactBinding;
import com.hithamsoft.learnapp.contacts2020.model.Contact;

public class AddContact extends AppCompatActivity implements View.OnClickListener {

    ActivityAddContactBinding contactBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactBinding= DataBindingUtil.setContentView(this,R.layout.activity_add_contact);
        getSupportActionBar().setTitle("Add Title");
       // getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        contactBinding.contactAddContactBtn.setOnClickListener(this);
        contactBinding.contactAddAnotherBtn.setOnClickListener(this);

    }

    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        contactBinding.addContactForm.setVisibility(show ? View.GONE : View.VISIBLE);
        contactBinding.addContactForm.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                contactBinding.addContactForm.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        contactBinding.contactProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        contactBinding.contactProgress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                contactBinding.contactProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });


    }
    private boolean validateForm() {
        boolean valid = true;



        String contactName = contactBinding.contactNameTxt.getText().toString();
        String contactNumber = contactBinding.contactNumberTxt.getText().toString();
        String contactEmail = contactBinding.contactEmailTxt.getText().toString();
        if (TextUtils.isEmpty(contactName)) {
            contactBinding.contactNameTxt.setError("Required.");
            valid = false;
        } else {
            contactBinding.contactNameTxt.setError(null);
        }


        if (TextUtils.isEmpty(contactNumber)) {
            contactBinding.contactNumberTxt.setError("Required.");
            valid = false;
        } else {
            contactBinding.contactNumberTxt.setError(null);
        }


        if (TextUtils.isEmpty(contactEmail)) {
            contactBinding.contactEmailTxt.setError("Required.");
            valid = false;
        } else {
            contactBinding.contactEmailTxt.setError(null);
        }

        return valid;
    }
    private void addContact(){
        if (!validateForm()){
            return;

        }
        String contactName = contactBinding.contactNameTxt.getText().toString();
        String contactNumber = contactBinding.contactNumberTxt.getText().toString();
        String contactEmail = contactBinding.contactEmailTxt.getText().toString();
        Contact addContact=new Contact();
        addContact.setName(contactName);
        addContact.setNumber(contactNumber);
        addContact.setEmail(contactEmail);
        addContact.setNameChar(contactName);
        addContact.setUserEmail(App.user.getEmail());

        showProgress(true);//show progress after send data
        Backendless.Persistence.save(addContact, new AsyncCallback<Contact>() {
            @Override
            public void handleResponse(Contact response) {
                showProgress(false);
                Toast.makeText(AddContact.this, "Contact Added successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                showProgress(false);
                Toast.makeText(AddContact.this, "Error: "+fault.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void addAnother(){
        contactBinding.contactNameTxt.setText("");
         contactBinding.contactNumberTxt.setText("");
        contactBinding.contactEmailTxt.setText("");
    }

    @Override
    public void onClick(View v) {

        if (v.getId()==contactBinding.contactAddContactBtn.getId()){
            addContact();
        }else
            if (v.getId()==contactBinding.contactAddAnotherBtn.getId()){
         addAnother();
        }

    }
}
