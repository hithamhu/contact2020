package com.hithamsoft.learnapp.contacts2020;

public class UserInfo {
    String Hardware;
    String HardwareMeaning;

    public UserInfo() {
    }

    public UserInfo(String hardware, String hardwareMeaning) {
        Hardware = hardware;
        HardwareMeaning = hardwareMeaning;
    }

    public String getHardware() {
        return Hardware;
    }

    public void setHardware(String hardware) {
        Hardware = hardware;
    }

    public String getHardwareMeaning() {
        return HardwareMeaning;
    }

    public void setHardwareMeaning(String hardwareMeaning) {
        HardwareMeaning = hardwareMeaning;
    }
}
