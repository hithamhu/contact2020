package com.hithamsoft.learnapp.contacts2020.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.databinding.ActivityForgotPasswordBinding;

public class ForgotPassword extends AppCompatActivity {
    ActivityForgotPasswordBinding forgotPasswordBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgotPasswordBinding= DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);
        forgotPasswordBinding.resetPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });

    }

    private boolean validateForm() {
        boolean valid = true;



        String email = forgotPasswordBinding.resetPassEmailTxt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            forgotPasswordBinding.resetPassEmailTxt.setError("Required.");
            valid = false;
        } else {
            forgotPasswordBinding.resetPassEmailTxt.setError(null);
        }


        return valid;
    }
    private void resetPassword(){
        if (!validateForm()){
            return;
        }
        String email=forgotPasswordBinding.resetPassEmailTxt.getText().toString();
        Backendless.UserService.restorePassword(email, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Toast.makeText(ForgotPassword.this, "reset Link sent to your email \nCheck your email ", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ForgotPassword.this,Login.class));
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(ForgotPassword.this, "Error: "+fault.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
