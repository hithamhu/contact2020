package com.hithamsoft.learnapp.contacts2020.activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class userInfo {
    private static final String TAG = "userInfo";

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String[] getAndroidHardWare(Context context){
        String androidSDK = String.valueOf(android.os.Build.VERSION.SDK_INT);
        String androidVersion = android.os.Build.VERSION.RELEASE;
        String androidBrand = android.os.Build.BRAND;
        String androidManufacturer = android.os.Build.MANUFACTURER;
        String androidModel = android.os.Build.MODEL;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            @SuppressLint("MissingPermission") String androroidSerial = Build.getSerial();
        }
        WifiManager wifiManager = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        WifiInfo wInfo = wifiManager.getConnectionInfo();
        //  String macAddress = wInfo.getMacAddress();
        String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        String macAddress = getMacAddr();
        String deviceID[]=getTelphone(context);
        return new String[]{androidSDK, androidVersion, androidBrand, androidManufacturer, androidModel,macAddress,ip,deviceID[0],
                deviceID[1],deviceID[2],deviceID[3]};
    }
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif: all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b: macBytes) {
                    //res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG, "getMacAddr: "+res1.toString());
                return res1.toString();
            }
        } catch (Exception ex) {}
        return "02:00:00:00:00:00";
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private String[] getTelphone(Context context){
        String number[]=new String[8];
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        String deviceId;
        final TelephonyManager mTelephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            number[1] = mTelephony.getDeviceId();
            number[0]=mTelephony.getLine1Number();
            number[2]=mTelephony.getNetworkOperatorName();
            number[3]=mTelephony.getSimCountryIso();
            number[4]=mTelephony.getNetworkOperator();
            number[5]=mTelephony.getDeviceSoftwareVersion();
            number[6]=mTelephony.getManufacturerCode();
            number[7]=mTelephony.getNetworkSpecifier();
        }
        else {
            number[1] = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }

        return number;
    }

}
