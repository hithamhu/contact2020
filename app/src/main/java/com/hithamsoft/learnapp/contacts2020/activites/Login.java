package com.hithamsoft.learnapp.contacts2020.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.hithamsoft.learnapp.contacts2020.App;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.databinding.ActivityLoginBinding;

public class Login extends AppCompatActivity implements View.OnClickListener
{
    private static final String TAG = "Login";

    ActivityLoginBinding loginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginBinding= DataBindingUtil.setContentView(this,R.layout.activity_login);
        loginBinding.loginSignupBtn.setOnClickListener(this);
        loginBinding.loginForgetPassBtn.setOnClickListener(this);
        loginBinding.loginSignInBtn.setOnClickListener(this);

    }
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        loginBinding.loginFormContainer.setVisibility(show ? View.GONE : View.VISIBLE);
        loginBinding.loginFormContainer.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                loginBinding.loginFormContainer.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        loginBinding.loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        loginBinding.loginProgressBar.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                loginBinding.loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });


    }
    private boolean validateForm() {
        boolean valid = true;



        String email = loginBinding.loginEmailTxt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            loginBinding.loginEmailTxt.setError("Required.");
            valid = false;
        } else {
            loginBinding.loginEmailTxt.setError(null);
        }

        String password = loginBinding.loginPasswordTxt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            loginBinding.loginPasswordTxt.setError("Required.");
            valid = false;
        } else {
            loginBinding.loginPasswordTxt.setError(null);
        }

        return valid;
    }
    private void loginProcess(){

        if (!validateForm()){
            return;
        }
        showProgress(true);
        String email=loginBinding.loginEmailTxt.getText().toString();
        String password=loginBinding.loginPasswordTxt.getText().toString();
        Backendless.UserService.login(email, password, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                App.user=response;
                showProgress(false);
                startActivity(new Intent(Login.this,MainActivity.class));
                Login.this.finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(Login.this, "Something Wrong :( \n Error is: "+fault.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "handleFault: "+fault.getMessage());
                showProgress(false);
            }
        }, true);
    }



    @Override
    public void onClick(View v) {

        if(v.getId()==loginBinding.loginSignInBtn.getId()){
            loginProcess();

        }else if (v.getId()==loginBinding.loginForgetPassBtn.getId()){
            startActivity(new Intent(Login.this,ForgotPassword.class));

        }else if (v.getId()==loginBinding.loginSignupBtn.getId()){
            startActivity(new Intent(Login.this,Register.class));
            finish();

        }

    }
}
