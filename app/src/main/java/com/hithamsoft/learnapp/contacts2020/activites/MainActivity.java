package com.hithamsoft.learnapp.contacts2020.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.hithamsoft.learnapp.contacts2020.App;
import com.hithamsoft.learnapp.contacts2020.R;
import com.hithamsoft.learnapp.contacts2020.adapter.ContactAdapter;
import com.hithamsoft.learnapp.contacts2020.databinding.ActivityMainBinding;
import com.hithamsoft.learnapp.contacts2020.model.Contact;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    ActivityMainBinding activityMainBinding;
    public static List<Contact> contactList;
    public static ContactAdapter contactAdapter;
    boolean isContactAvilable=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        activityMainBinding.openAddContactBtn.setOnClickListener(this);
        getSupportActionBar().setTitle("Contact");
        activityMainBinding.noContactContainer.setVisibility(View.GONE);
        activityMainBinding.contactList.setLayoutManager(new LinearLayoutManager(this));
      //  activityMainBinding.contactList.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        contactList=new ArrayList<>();
        getContactData();
        isContactAvilable();


    }

    private void isContactAvilable(){
        if (!isNetworkAvailable()){
            activityMainBinding.progressBarId.setVisibility(View.GONE);
            activityMainBinding.inValidIcon.setImageResource(R.drawable.ic_no_wifi);
            activityMainBinding.inValidText.setText(getResources().getString(R.string.no_network));
            activityMainBinding.noContactContainer.setVisibility(View.VISIBLE);
        }else if (isContactAvilable){
            activityMainBinding.progressBarId.setVisibility(View.GONE);
            activityMainBinding.inValidIcon.setImageResource(R.drawable.ic_error);
            activityMainBinding.inValidText.setText(getResources().getString(R.string.no_contacts_create));
            activityMainBinding.noContactContainer.setVisibility(View.VISIBLE);
        }else {
            activityMainBinding.progressBarId.setVisibility(View.GONE);
            activityMainBinding.noContactContainer.setVisibility(View.GONE);
            activityMainBinding.contactList.setVisibility(View.VISIBLE);
        }



    }

    private void getContactData(){
        String whereCluse="userEmail = '"+ App.user.getEmail()+"'";
        DataQueryBuilder queryBuilder=DataQueryBuilder.create();
        queryBuilder.setWhereClause(whereCluse);
        queryBuilder.setGroupBy("name");
        activityMainBinding.progressBarId.setVisibility(View.VISIBLE);

        Backendless.Persistence.of(Contact.class).find(queryBuilder, new AsyncCallback<List<Contact>>() {
            @Override
            public void handleResponse(List<Contact> response) {
                activityMainBinding.progressBarId.setVisibility(View.GONE);
                activityMainBinding.noContactContainer.setVisibility(View.GONE);
                activityMainBinding.contactList.setVisibility(View.VISIBLE);
                isContactAvilable= !response.isEmpty();
                contactList.addAll(response);
                Log.d(TAG, "handleResponse: list "+response.size());
             contactAdapter=new ContactAdapter(MainActivity.this, contactList);

             activityMainBinding.contactList.setAdapter(contactAdapter);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                Toast.makeText(MainActivity.this, "Error: "+fault.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // contactAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==activityMainBinding.openAddContactBtn.getId()){
            startActivity(new Intent(MainActivity.this,AddContact.class));
        }
    }




}
