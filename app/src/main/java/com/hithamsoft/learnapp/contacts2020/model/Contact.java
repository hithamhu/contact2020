package com.hithamsoft.learnapp.contacts2020.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Contact implements Parcelable {

    private String name;
    private String number;
    private String email;
    private Date created;
    private Date updated;
    private String objectId;
    private String userEmail;
    private String nameChar;

    protected Contact(Parcel in) {
        name = in.readString();
        number = in.readString();
        email = in.readString();
        objectId = in.readString();
        userEmail = in.readString();
        nameChar = in.readString();
    }

    public Contact() {
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getNameChar() {
        return String.valueOf(nameChar.toUpperCase().charAt(0));
    }

    public void setNameChar(String nameChar) {
        this.nameChar = nameChar;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(email);
        dest.writeString(objectId);
        dest.writeString(userEmail);
        dest.writeString(nameChar);
    }
}
